const App = require('./app');
const constants = require('./constants');


module.exports = {
    RESET: "\x1b[0m",

    CODE: {
        SERVER: 100,
        REQUEST: 200,
        PROMISE: 300
    },


    STYLE: {
        BRIGHT: "\x1b[1m",
        DIM: "\x1b[2m",
        UNDERSCORE: "\x1b[4m",
        BLINK: "\x1b[5m",
        REVERSE: "\x1b[7m",
        HIDDEN: "\x1b[8m"
    },
    
    
    COLOR: {
        BLACK: "\x1b[30m",
        RED: "\x1b[31m",
        GREEN: "\x1b[32m",
        YELLOW: "\x1b[33m",
        BLUE: "\x1b[34m",
        MAGENTA: "\x1b[35m",
        CYAN: "\x1b[36m",
        WHITE: "\x1b[37m"
    },

    
    BACKGROUND: {
        BLACK: "\x1b[40m",
        RED: "\x1b[41m",
        GREEN: "\x1b[42m",
        YELLOW: "\x1b[43m",
        BLUE: "\x1b[44m",
        MAGENTA: "\x1b[45m",
        CYAN: "\x1b[46m",
        WHITE: "\x1b[47m"
    },
    
    
    error(code, ...message) {
        this._paint('error', code, ...message);
    },


    warning(code, ...message) {
        this._paint('warning', code, ...message);
    },


    message(code, ...message) {
        this._paint('log', code, ...message);
    },


    make(...msg) {
        return msg.join('\n');
    },


    style(string, color='', background='', style='') {
        if (App.property('space') === constants.SPACE_SERVER) {
            return [background, color, style, string, this.RESET].join('');
        }

        return string;
    },


    _paint(type, code, ...message) {
        let parent = module.parent ? module.parent.filename : '';
        let params = App.property('LOGGER') || [];

        if (params.includes(code)) {
            console[type](this.make(
                this.style(parent, '', '', this.STYLE.DIM),
                ...message.map(item => {
                    return this.style(item);
                })
            ));
        }
    }
};