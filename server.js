const http = require('http');
const url = require('url');

const App = require('./app');
const SystemRequest = require('./system/system-request');
const Request = require('./request');
const constants = require('./constants');
const logger = require('./logger');
const env = require('./server/nunjucks-loader');

const write_file = require('./server/write-file');
const make = require('./system/make');
const system_server_request = require('./system/system-server-request');
const system_error_page = require('./system/system-error-page');
const read_config = require('./system/read-config');

let server;


Request._cli = system_server_request;


App.property('env', constants.ENV_SERVER);


App.property('space', constants.SPACE_SERVER);


module.exports = {

    config: {},


    static_path: '/static/',


    /**
     * Читает и добавляет конфиг в параметры сервера
     * @param argv
     */
    set_config: function (...argv) {
        Object.assign(this.config, read_config(...argv));
    },


    /**
     * Предворительная обработка страниц
     * @param content
     * @param context
     * @returns {*}
     */
    prepare_page(content, context, response) {
        return content;
    },


    /**
     * Запуск сервера
     * @param port
     * @param host
     */
    run(port=5000, host='127.0.0.1') {
        this.get_server().listen(port, host);
    },


    /**
     * Получение ссылки и старт сервера
     * @returns {*}
     */
    get_server() {
        if (server) {
            return server;
        }

        return server = http.createServer((req, res) => {
            let path = url.parse(req.url).path;

            if (path.includes(this.static_path) || path.includes('.ico')) {
                write_file(req, res);

            } else {
                var body = [];

                App.response = [];

                req.on('error', function(err) {
                    logger.error(logger.CODE.SERVER, err);

                }).on('data', function(chunk) {
                    body.push(chunk);

                }).on('end', () => {
                    let request;
                    let catch_end = err => {
                        res.end('' + err);
                        logger.error(logger.CODE.SERVER, err);
                    };
                    let then_end = response => {
                        let cookie = request.cookie.toList();

                        let headers = {
                            'Content-Type': 'text/html',
                            'Accept-Ranges': 'bytes',
                            'Cache-Control': 'no-cache'
                        };

                        if (cookie.length) {
                            headers['Set-Cookie'] = cookie;
                        }

                        res.writeHead(response.code, Object.assign(headers, response.headers));

                        res.end(
                            this.prepare_page(
                                response.get_content(request.type),
                                App.get_context(),
                                response
                            )
                        );
                    };

                    body = Buffer.concat(body).toString();
                    request = new SystemRequest(req, body);

                    App.clear_context();
                    App.property('path', path);

                    make(path, request).then(response => {
                        if (response.code < 400) {
                            then_end(response);
                        } else {

                            system_error_page(request, 404)
                                .then(then_end)
                                .catch(catch_end);
                        }
                    }).catch(catch_end);
                });
            }
        });
    }
};
