const clear_slashes = require('./helper/clear-slashes');
const each = require('./helper/each');
const is_string = require('./helper/is-string');
const is_number =require('./helper/is-number');


const URL_PARAM_REGEXP = new RegExp('<(.*)>', 'i');


module.exports = {

    /**
     * Обязательный последний слеш
     */
    is_last_slashes: true,


    /**
     * Список роутов
     */
    _route_list: {},


    _error_handlers: {},


    /**
     * Установка обработчика страници ошибки
     * @param handler
     * @param code
     */
    error_handler(handler, code) {
        if (!this._error_handlers[code]) {
            this._error_handlers[code] = [];
        }

        this._error_handlers[code].push(handler);
    },


    /**
     * Проверка на существование url в списке
     * @param url
     * @returns {{route: boolean, params: boolean}}
     */
    check(url) {
        let url_parts = this._parse_path(url);
        let keys = Object.keys(this._route_list);
        let i = 0;
        let result = [];


        for(; i < keys.length; i++) {
            let key = keys[i];
            let item = this._route_list[key];

            if (item.is_regexp) {
                if (item.route.test(url)) {
                    result.push({
                        params: Array.prototype.slice.call(url.match(item.route)),
                        handlers: item.handlers
                    });
                }
            } else {
                let params = this._test_route(item, url_parts);

                if (params) {
                    result.push({
                        params: params,
                        handlers: item.handlers
                    })
                }
            }
        }

        return result;
    },


    /**
     * Добавление роута в список
     * @param route
     * @param handler
     */
    add(route, handler) {
        let route_name = route;
        let is_regexp = false;

        if (route instanceof RegExp) {
            route_name = route.toString();
            is_regexp = true;
        }

        if (!this._route_list[route_name]) {
            let parts;

            if (!is_regexp) {
                parts = this._parse_path(route)
            }

            this._route_list[route_name] = {
                is_regexp: is_regexp,
                route: route,
                parts: parts,
                handlers: []
            }
        }



        this._route_list[route_name].handlers.push(handler);
    },


    /**
     * Добавить список роутов
     * @param route_list
     */
    add_list(route_list) {
        each(Object.keys(route_list), route => {
            this.add(route, route_list[route]);
        });
    },


    /**
     * Уберает лишние слешы из пути
     * @param path
     * @returns {string}
     */
    check_slashes(path) {
        let last_slash = '';

        if (this.is_last_slashes && path.substr(-1) == '/') {
            last_slash = '/';
        }

        return clear_slashes(path) + last_slash;
    },


    _parse_path(path) {
        path = this.check_slashes(path);
        let parts = [];

        each(path.split('/'), item => {
            let variable;
            let type;

            if (item.match(URL_PARAM_REGEXP)) {
                variable = item.replace(/[<>\n]/g, '');
                variable = variable.split(':');
                type = variable[1];
                variable = variable[0];
            }

            parts.push({
                base: item,
                variable: variable,
                type: type
            })
        });

        return parts;
    },


    _test_route(route, parts) {
        let params = false;

        if (route.parts.length === parts.length) {
            params = {};

            each(route.parts, (route_part, index) => {
                if (route_part.variable) {
                    if (!this._test_type(route_part.type, parts[index].base)) {
                        return params = false;
                    }

                    params[route_part.variable] = parts[index].base;

                } else {
                    if (route_part.base !== parts[index].base) {
                        return params = false;
                    }
                }
            });
        }

        return params;
    },


    _test_type(type, value) {
        switch (type) {
            case 'string':
                return !is_number(value) && is_string(value);
            break;

            case 'number':
                return is_number(value);
            break;
        }

        return true;
    }
};