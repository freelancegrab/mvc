const Request = require('./request');
const each = require('./helper/each');
const is_string = require('./helper/is-string');


module.exports = class Model {
    constructor() {}


    get(body) {
        return Request.get(this._options['get'], {
            body: body
        });
    }


    post(body) {
        return Request.post(this._options['post'], {
            body: body
        });
    }


    put(body) {
        return Request.put(this._options['put'], {
            body: body
        });
    }


    delete(body) {
        return Request.delete(this._options['put'], {
            body: body
        });
    }


    static create(options, model={}) {
        let opt = {
            get: '',
            post: '',
            put: '',
            'delete': ''
        };

        if (is_string(options)) {
            options = {
                get: options,
                post: options,
                put: options,
                'delete': options
            };
        }

        each(Object.keys(options), key => {
            if (['get', 'post', 'put', 'delete'].includes(key) && is_string(options[key])) {
                opt[key] = options[key];
            }
        });

        model.prototype._options = opt;
        return new model;
    }
};
