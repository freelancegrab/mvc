require('./polyfils');

const App = require('./app');
const constants = require('./constants');
const SystemRequest = require('./system/system-request');
const SystemResponse = require('./system/system-response');
const Request = require('./request');

const make = require('./system/make');
const closest = require('./helper/closest');
const each = require('./helper/each');
const system_client_request = require('./system/system-client-request');
const system_error_page = require('./system/system-error-page');


Request._cli = system_client_request;


App.attach_event('remove_cookie', cookie => {
    set_cookie(cookie.key, cookie.value, cookie.options);
});


App.attach_event('set_cookie', cookie => {
    set_cookie(cookie.key, cookie.value, cookie.options);
});


App.attach_event('redirect', path => {
    window.location = path;
});


App.attach_event('refresh', () => {
    window.location.reload();
});


App.property('space', constants.SPACE_CLIENT);


module.exports = {

    _point_start: undefined,


    _referer: undefined,


    body: document.body,


    /**
     * Старт приложения
     */
    run() {
        this._point_start = App.property('env');
        App.property('path', window.location.pathname + window.location.search);
        this.check(window.location.pathname + window.location.search);
        this.listen();
    },


    check(path) {
        let request;
        let catch_end = err => {
            App.dispatch_event('page_error', [err]);
        };
        let then_end = response => {
            if (App.property('env') === constants.ENV_CLIENT) {
                this.body.innerHTML = response.get_content(constants.REQUEST_CONTENT_TYPE_HTML);
            }

            App.property('env', constants.ENV_CLIENT);

            App.component.prepare();

            App.dispatch_event('page_ready', [response]);
        };

        App.clear_context();
        App.property('path', path);

        request = new SystemRequest({
            url: window.location.pathname,
            method: 'GET',
            referer: this._referer,
            headers: {
                cookie: document.cookie
            }
        });


        this._referer = path;

        make(path, request).then(response => {
            if (App.property('env') === constants.ENV_ISOMORPHIC) {
                then_end(new SystemResponse('', 200));
            } else {
                if (response.code === constants.REQUEST_CODE_SUCCESS) {
                    then_end(response);
                } else {
                    system_error_page(request, 404)
                        .then(then_end)
                        .catch(catch_end);
                }
            }
        }).catch(catch_end);
    },


    listen() {
        let pushState = history.pushState;

        history.pushState = function(state) {
            let ps = pushState.apply(history, arguments);
            if (typeof history.onpushstate == "function") {
                history.onpushstate({state: state});
            }
            return ps;
        };

        document.removeEventListener('click', this._history_target.bind(this));
        document.addEventListener('click', this._history_target.bind(this));

        window.onpopstate = history.onpushstate = function (data) {
            let path = window.location.pathname + window.location.search;
            let hash = window.location.hash;

            if (data.state && data.state.stopPropagation) {
                return;
            }

            if (hash) {
                App.dispatch_event('change_hash', [hash]);
                return;
            }

            if (this._point_start === constants.ENV_SERVER) {
                return;
            }

            if(path !== App.property('path')) {
                App.dispatch_event('change_url', [path]);
                this.check(path);
            }
        }.bind(this);
    },

    _history_target(event) {
        let a = closest(event.target, 'a');

        if (!a) {
            return;
        }

        if (window.location.host !== a.host) {
            return;
        }

        if (a.hasAttribute('target') && ['_blank', '_parent', '_top'].includes(a.getAttribute('target'))) {
            return;
        }

        if (this._point_start === constants.ENV_SERVER) {
            return;
        }

        if (event.metaKey || event.button !== 0) {
            return;
        }

        event.preventDefault();
        history.pushState(null, null, a.getAttribute('href'));
    }
};


/**
 * Установка Cookie
 * @param name
 * @param value
 * @param options
 */
function set_cookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}