const EventEmitter = require('./event-emitter');
const each = require('./helper/each');
const ComponentCollection = require('./component-collection');

const EVENTS = new EventEmitter();


module.exports = {
    component: ComponentCollection,


    /**
     * Список контроллеров загруженных на текущую страницу
     */
    current_controllers: [],


    /**
     * Неизменяемые данные страници
     */
    _scope: {
        debug: false,
        env: 'client',
        path: ''
    },


    /**
     * Контекст текущей страници
     *
     */
    _context: {},


    /**
     * Установка контекста
     * @param context
     */
    set_context(context) {
        return Object.assign(this._context, context);
    },


    /**
     * Получение контекста
     * @returns {*}
     */
    get_context() {
        return Object.assign({}, this._context);
    },


    /**
     * Очистка контекста
     */
    clear_context() {
        this._context = {};
    },


    /**
     * Устанавливаем и получаем значение из общего скопа приложения
     * @param name
     * @param value
     * @returns {*}
     */
    property(name, value) {
        return this._scope[name] = value !== undefined ? value : this._scope[name];
    },


    /**
     * Возбудить глобальное событие
     * @param event_name
     * @param params
     */
    dispatch_event: function(event_name, params=[]) {
        EVENTS.dispatch(event_name, params);
    },


    /**
     * Подписаться на глобальное событие
     * @param event_name
     * @param handler
     */
    attach_event: function(event_name, handler) {
        EVENTS.attach(event_name, handler)
    },


    /**
     * Удалить обработчик глобальных события
     * @param event_name
     * @param handler
     */
    remove_event: function (event_name, handler) {
        EVENTS.remove(event_name, handler);
    }
};
