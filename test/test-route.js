var assert = require('assert');
var Route = require('../route');


describe('Route', function() {
    let handler = function () {
        return 1;
    };


    describe('#Secondary functions', function() {

        it('check_slashes', function () {

            Route.is_last_slashes = false;
            assert.ok(Route.check_slashes('/a') === 'a');

            Route.is_last_slashes = false;
            assert.ok(Route.check_slashes('/a/') === 'a');

            Route.is_last_slashes = true;
            assert.ok(Route.check_slashes('/a') === 'a');

            Route.is_last_slashes = true;
            assert.ok(Route.check_slashes('/a/') !== 'a');

        });

        it('_parse_path', function () {

            Route.is_last_slashes = false;
            assert.ok(Route._parse_path('/a/').length === 1);

            Route.is_last_slashes = false;
            assert.ok(Route._parse_path('/a').length === 1);

            Route.is_last_slashes = true;
            assert.ok(Route._parse_path('/a/').length === 2);

            Route.is_last_slashes = true;
            assert.ok(Route._parse_path('/a').length === 1);

        });
    });


    describe('#Test last slashes', function() {
        it('Slashes: True', function () {

            Route._route_list = {};
            Route.is_last_slashes = true;
            Route.add('/a');

            assert.ok(Route.check('/a').length === 1);
            assert.ok(Route.check('/a/').length === 0);

        });


        it('Slashes: False', function () {

            Route._route_list = {};
            Route.is_last_slashes = false;
            Route.add('/a');

            assert.ok(Route.check('/a').length === 1);
            assert.ok(Route.check('/a/').length === 1);

        });
    });


    describe('#check', function() {

        it('Get empty params: /a/b/', function() {

            Route._route_list = {};
            Route.add('/a/b/', handler);

            assert.ok(
                Route.check('/a/b/').length === 1 &&
                Route.check('/a/b/')[0].params instanceof Object
            );

        });

        it('Get params: /a/<b>/', function () {

            Route._route_list = {};
            Route.add('/a/<b>/', handler);

            assert.ok(
                Route.check('/a/b/').length === 1 &&
                Route.check('/a/b/')[0].params.b === 'b'
            );

        });

        it('Get params list: RegExp(^/a/([a-zA-Z0-9]+)/$)', function () {

            Route._route_list = {};
            Route.add(new RegExp('^/a/([0-9]+)/$'), handler);

            assert.ok(
                Route.check('/a/1/').length === 1 &&
                Route.check('/a/1/')[0].params[1] === '1'
            );

        });


        it('Get handler list', function () {

            Route._route_list = {};
            Route.add('/', handler);

            assert.ok(
                Route.check('/').length === 1 &&
                Route.check('/')[0].handlers.length === 1
            );

        });


        it('Test list routes', function () {

            Route._route_list = {};
            Route.add_list({
                '/': handler,
                '/a': handler,
                '/a/<b>/': handler
            });

            Route.add(new RegExp('^/$'), handler);
            Route.add(new RegExp('^/a$'), handler);
            Route.add(new RegExp('^/a/([a-zA-Z0-9]+)/$'), handler);

            assert.ok(Route.check('/').length === 2);
            assert.ok(Route.check('/a').length === 2);
            assert.ok(Route.check('/a/b/').length === 2);
        });


        it('Test types number', function () {

            Route._route_list = {};
            Route.add('/a/<b:number>/', handler);

            assert.ok(Route.check('/a/1/').length === 1);
            assert.ok(Route.check('/a/b/').length === 0);
        });


        it('Test types string', function () {

            Route._route_list = {};
            Route.add('/a/<b:string>/', handler);

            assert.ok(Route.check('/a/1/').length === 0);
            assert.ok(Route.check('/a/b/').length === 1);
        });
    });
});
