const assert = require('assert');
const each = require('../helper/each');
const is_array = require('../helper/is-array');
const parse_json = require('../helper/parse-json');
const parse_query = require('../helper/parse-query');
const clear_slashes = require('../helper/clear-slashes');

describe('Helpers', function () {
    
    describe('#each', function () {
        
        it('Returns length of given array', function () {
            let is = 0;
            each([1, 2, 3, 4], (item) => {
                is++
            });
            assert(is, 4);
        });
        
    });
    
    
    describe('#parse_json', function () {
        
        it('Returns JSON object from given string', function () {
            let string = '{"1": 1, "2": 2, "3": {"4": 4, "5": {"6": 6}}}';
            let result = {
                '1': 1,
                '2': 2,
                '3': {
                    '4': 4,
                    '5': {
                        '6': 6
                    }
                }
            };
            
            assert(parse_json(string), result);
        });
        
        
        it('Returns empty object from given string', function () {
            assert(parse_json('some string'), {});
        });
        
        
        it('Return same number from given number', function () {
            assert(parse_json(3251235), 3251235);
        });
    });
    
    
    describe('#parse_query', function () {
        
        it('Returns JSON object from given query string', function () {
            let query = 'sourceid=chrome-instant&rlz=1C5CHFA_enRU727RU727&ion=1';
            let json = {
                sourceid: 'chrome-instant',
                rlz: '1C5CHFA_enRU727RU727',
                ion: '1'
            };
            
            assert(parse_query(query), json);
        });
        
        
        it('Returns JSON object from given query string', function () {
            let query = '?sourceid=chrome-instant&rlz=1C5CHFA_enRU727RU727&ion=1&';
            let json = {
                sourceid: 'chrome-instant',
                rlz: '1C5CHFA_enRU727RU727',
                ion: '1'
            };
            
            assert(parse_query(query), json);
        });
        
    });
    
    
    describe('#clear_slashes', function () {
        
        it('Returns string w/o slashes at the beginning and the end', function () {
            assert(clear_slashes('/api/external-project/list/'), 'api/external-project/list');
        });
        
        
        it('Returns string w/o slashes at the beginning and the end', function () {
            assert(clear_slashes('////api/external-project/list////'), 'api/external-project/list');
        });
        
        
        it('Returns string w/o slashes at the beginning and the end', function () {
            assert(clear_slashes('////////////////////'), '');
        });
        
        
        it('Returns string from given number', function () {
            assert(clear_slashes(1523512351235), '1523512351235');
        });
        
        
        it('Returns string from given array', function () {
            assert(clear_slashes([1,2,3,4]), "1,2,3,4");
        });
        
    });
    
    
    describe('#is_array', function () {
        
        it('Returns [object Array] for given argument', function () {
            assert(is_array([1, 2, 3, 4]), '[object Array]');
        });
        
        
        it('Returns [object Array] for given argument', function () {
            assert(is_array(['a', 'b', 'c', 'd']), '[object Array]');
        });
        
        
        it('Returns [object Array] for given argument', function () {
            assert(is_array([4124, 'asda', 'gasda', 51235]), '[object Array]');
        });
        
        
        it('Returns [object Array] for given argument', function () {
            assert(is_array([
                {
                    'a': 'b',
                    'c': 'd'
                },
                {
                    'e': 'f',
                    'g': 'h'
                }
            ]), '[object Array]');
        });
        
    });
    
});
