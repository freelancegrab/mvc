var assert = require('assert');
var read_config = require('../system/read-config');


describe('System read config', function() {

    it('List configs', function () {
        let config = read_config(
            __dirname + '/read-config-test/read-config-test.js',
            __dirname + '/read-config-test/read-config-test.json',
            __dirname + '/read-config-test/read-config-test.yaml'
        );

        assert.ok(config.json && config.js && config.yaml)
    });

    it('Merge dict', function () {
        let config = read_config(
            __dirname + '/read-config-test/read-config-test.js',
            {
                custom: 1
            }
        );

        assert.ok(config.js && config.custom)
    });
});