var assert = require('assert');
var Route = require('../route');
var make = require('../system/make');


describe('System make', function() {
    var handler = (resolve, reject) => {
        resolve(1);
    };


    describe('#check', function () {

        it('not found', function (done) {
            Route.is_last_slashes = true;
            Route._route_list = {};

            make('a').then(function (response) {
                assert.ok(response.code === 404);
                done();
            });
        });


        it('success', function (done) {
            Route.is_last_slashes = true;
            Route._route_list = {};

            Route.add('/a', handler);

            make('/a').then(function (response) {
                assert.ok(
                    response.code === 200 &&
                    response.body.html[0] === '1'

                );
                done();
            });
        });


        it('throw', function (done) {
            Route.is_last_slashes = true;
            Route._route_list = {};

            Route.add('/a', function (resolve, reject) {
                throw 'Error';
            });

            make('/a').then(function () {
                assert.ok(false);
                done();
            }).catch(function () {
                assert.ok(true);
                done();
            });
        });


        it('multiple handler', function (done) {
            Route.is_last_slashes = true;
            Route._route_list = {};

            Route.add('/a', handler);
            Route.add('/a', handler);
            Route.add('/a', handler);

            make('/a').then(function (response) {
                assert.ok(response.body.html.length === 3);
                done();
            });
        });


        it('mixed multiple handler', function (done) {
            Route.is_last_slashes = true;
            Route._route_list = {};

            Route.add('/a', handler);
            Route.add(new RegExp('^/a$'), handler);
            Route.add('/a', handler);

            make('/a').then(function (response) {
                assert.ok(response.body.html.length === 3);
                done();
            });
        });

    });

});