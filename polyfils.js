if (!Object.assign) {
    Object.defineProperty(Object, 'assign', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: function(target, firstSource) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert first argument to object');
            }

            var to = Object(target);
            for (var i = 1; i < arguments.length; i++) {
                var nextSource = arguments[i];
                if (nextSource === undefined || nextSource === null) {
                    continue;
                }

                var keysArray = Object.keys(Object(nextSource));
                for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                    var nextKey = keysArray[nextIndex];
                    var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                    if (desc !== undefined && desc.enumerable) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
            return to;
        }
    });
}

function f() {}
if (!f.name) {
    Object.defineProperty(Function.prototype, 'name', {
        get: function() {
            var name = this.toString().match(/^function\s*([^\s(]+)/)[1];
            Object.defineProperty(this, 'name', { value: name });
            return name;
        }
    });
}

if (!FormData.prototype.delete) {
    FormData.prototype.delete = function () {
        return;
    };
}

if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.matchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        Element.prototype.mozMatchesSelector ||
        Element.prototype.msMatchesSelector;
}

Number.isFinite = Number.isFinite || function(value) {
    return typeof value === 'number' && isFinite(value);
};


if (![].includes) {
    Array.prototype.includes = function (searchElement) {
        'use strict';
        var O = Object(this);
        var len = parseInt(O.length) || 0;
        if (len === 0) {
            return false;
        }
        var n = parseInt(arguments[1]) || 0;
        var k;
        if (n >= 0) {
            k = n;
        } else {
            k = len + n;
            if (k < 0) {
                k = 0;
            }
        }
        while (k < len) {
            var currentElement = O[k];
            if (searchElement === currentElement ||
                (searchElement !== searchElement && currentElement !== currentElement)
            ) {
                return true;
            }
            k++;
        }
        return false;
    };
}


if (!String.prototype.includes) {
    String.prototype.includes = function () {
        'use strict';
        return String.prototype.indexOf.apply(this, arguments) !== -1;
    };
}


if (!window.Promise) {
    window.Promise = require('promise-polyfill')
}


if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.matchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        Element.prototype.mozMatchesSelector ||
        Element.prototype.msMatchesSelector;
}

(function (arr) {
    arr.forEach(function (item) {
        if (item.hasOwnProperty('remove')) {
            return;
        }
        Object.defineProperty(item, 'remove', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: function remove() {
                this.parentNode.removeChild(this);
            }
        });
    });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);


if (self.document && !("insertAdjacentHTML" in document.createElementNS("http://www.w3.org/1999/xhtml", "_"))) {

    HTMLElement.prototype.insertAdjacentHTML = function (position, html) {
        "use strict";

        var
            ref = this
            , container = ref.ownerDocument.createElementNS("http://www.w3.org/1999/xhtml", "_")
            , ref_parent = ref.parentNode
            , node, first_child, next_sibling
        ;

        container.innerHTML = html;

        switch (position.toLowerCase()) {
            case "beforebegin":
                while ((node = container.firstChild)) {
                    ref_parent.insertBefore(node, ref);
                }
                break;
            case "afterbegin":
                first_child = ref.firstChild;
                while ((node = container.lastChild)) {
                    first_child = ref.insertBefore(node, first_child);
                }
                break;
            case "beforeend":
                while ((node = container.firstChild)) {
                    ref.appendChild(node);
                }
                break;
            case "afterend":
                next_sibling = ref.nextSibling;
                while ((node = container.lastChild)) {
                    next_sibling = ref_parent.insertBefore(node, next_sibling);
                }
                break;
        }
    };

}


if (self.document && !("insertAdjacentElement" in document.createElementNS("http://www.w3.org/1999/xhtml", "_"))) {

    HTMLElement.prototype.insertAdjacentElement = function (position, elem) {
        "use strict";

        let _this = this,
            parent = _this.parentNode,
            node, first, next;

        switch (position.toLowerCase()) {
            case 'beforebegin':
                while ((node = elem.firstChild)) {
                    parent.insertBefore(node, _this);
                }
                break;
            case 'afterbegin':
                first = _this.firstChild;
                while ((node = elem.lastChild)) {
                    first = _this.insertBefore(node, first);
                }
                break;
            case 'beforeend':
                while ((node = elem.firstChild)) {
                    _this.appendChild(node);
                }
                break;
            case 'afterend':
                parent.insertBefore(elem, _this.nextSibling);
                break;
        }
        return elem;

    }
}