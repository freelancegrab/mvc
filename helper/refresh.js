const App = require('../app');
const constants = require('../constants');
const redirect = require('./redirect');


module.exports = function refresh(request) {
    if (App.property('space') === constants.SPACE_CLIENT) {
        App.dispatch_event('refresh', [App.response.referer]);

        return false;
    }

    return redirect(request.referer);
};