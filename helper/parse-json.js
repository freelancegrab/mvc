/**
 * Получение JSON из строки, если строка невалидная вернется пустой объект
 * @param string
 * @returns {{}}
 */
module.exports = function parse_json(string) {
    try {
        return JSON.parse(string);
    } catch (e) {
        return {}
    }
};