const App = require('../app');
const constants = require('../constants');
const SystemResponse = require('../system/system-response');


module.exports = function redirect(path, code) {
    let access_code = [301, 302, 303, 305, 307];
    let headers = {
        'Location': path,
        'mimetype': 'text/html'
    };
    let body = [
        '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">\n',
        '<title>Redirecting...</title>\n',
        '<h1>Redirecting...</h1>\n',
        '<p>You should be redirected automatically to target URL: ',
        '<a href="%s">%s</a>.  If not click the link.'
    ].join('').replace(/%s/, path)

    code = code || 302;


    if (!access_code.includes(code)) {
        throw 'Redirect error code: ' + code + ' is not supported';
    }

    if (App.property('space') === constants.SPACE_CLIENT) {
        App.dispatch_event('redirect', [path]);
        return false;
    }

    return new SystemResponse(body, code, headers);
};