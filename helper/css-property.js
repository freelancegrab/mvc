/**
 * Получение css свойства
 * @param element
 * @param property
 * @returns {string}
 */
module.exports = function(element, property) {
    return window.getComputedStyle(element, null).getPropertyValue(property);
};