const each = require('./each');
const unwrap = require('./unwrap');

/**
 * Очистка от нежелательных тегов
 * @param parent_node
 * @param allowed
 */
module.exports = function clear_html_string(parent_node, allowed=[], allowed_attributes=[]) {
    each(parent_node.children, item => {
        clear_html_string(item, allowed, allowed_attributes);

        if (allowed.indexOf(item.tagName) < 0) {
            unwrap(item);
        } else {
            each(item.attributes, attr => {
                if (allowed_attributes.indexOf(attr.name) < 0) {
                    item.removeAttributeNode(attr);
                }
            });
        }
    });
};