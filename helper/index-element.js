/**
 * Получение индекса элемента внутри родителя
 * @param node
 * @returns {Number|number|*}
 */
module.exports = function index_element(node) {
    return node ? Array.prototype.slice.call( node.parentNode.children ).indexOf (node) : -1;
};