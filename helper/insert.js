const App = require('../app');


/**
 * Вставка html в DOM с последующим поиском компонет
 * @param parent_node
 * @param html_string
 * @param to
 */
module.exports = function insert(parent_node, html_string, to=false) {
    parent_node.insertAdjacentHTML(to ? 'afterBegin' : 'beforeEnd', html_string);

    App.component.prepare(parent_node);
};
