/**
 * Получить всех предков компонент
 * @param node
 * @returns {Array}
 */
module.exports = function parents_component(node) {
    let selector = '.component';
    let result = [];

    node = node.parentElement;
    while(node) {
        if (node.matches(selector)) {
            result.push(node.__components__ || []);
        }

        node = node.parentElement;
    }

    return result
};