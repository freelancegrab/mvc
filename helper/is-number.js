module.exports = function (value) {
    return Number.isFinite(value * 1);
};