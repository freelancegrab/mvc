const each = require('./each');

/**
 * Удалить обертку у элемента
 * @param element
 */
module.exports = function (element) {
    each(element.childNodes, child => {
        element.parentNode.insertBefore(child, element);
    });
    element.parentNode.removeChild(element);
};