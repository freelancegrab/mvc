/**
 * Поиск ноды по селектору от текущей ноды вверх, с перебором всех родителей
 * @param node
 * @param selector
 * @returns {*}
 */
module.exports = function closest(node, selector) {
    let result = null;

    while(node) {
        if (node.matches(selector)) {
            result = node;
            break;
        }

        node = node.parentElement;
    }

    return result;
};