const each = require('./each');


module.exports = function dict_to_query_string(dict, prefix) {
    let query = [];

    each(Object.keys(dict), item => {
        let prefix_key = isNaN(item) ? item : '';
        let key = prefix ? prefix + "[" + prefix_key + "]" : item;
        let value = dict[item];
        let chunk;

        if (value !== null && typeof value === "object") {
            chunk = dict_to_query_string(value, key);
        } else {
            chunk = encodeURIComponent(key) + "=" + encodeURIComponent(value);
        }

        query.push(chunk);
    });

    return query.join("&");
};