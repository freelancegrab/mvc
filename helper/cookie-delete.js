const cookie_set = require('./cookie-set');

/**
 * Обертка над document.cookie
 * @param name
 */

module.exports = function cookie_delete(name) {
    cookie_set(name, "", {
        expires: -1
    })
};