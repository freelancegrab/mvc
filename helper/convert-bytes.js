/**
 * Преобразование байт
 * @param bytes
 * @returns {*}
 */
module.exports = function(bytes) {
    let sizes = ['Байт', 'Кб', 'Мб', 'Гб', 'Тб'];
    let i;

    if (bytes == 0) {
        return '0 Byte';
    }

    i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};