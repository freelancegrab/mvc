module.exports = function history_change(url) {
    if (history.pushState) {
        history.pushState({
            stopPropagation: 1
        }, null, url);
    }
};