const EventMachine = require('./event-machine');
const each = require('./helper/each');
const create_component = require('./helper/create-component');
const parents_component = require('./helper/parents-component');
const find_components = require('./helper/find-components');


module.exports = class ObjectMachine extends EventMachine {
    constructor(node) {
        super();

        if (node) {
            this.node = node;
            this.name = node.getAttribute('name');
            create_component(node, this);
        }
    }

    /**
     * Получить все потомков
     * @returns {*}
     */
    get children_list() {
        if (this.node) {
            this._child_components = find_components(this.node);
        }
        return this._child_components;
    }


    /**
     * Получить всех предков
     * @returns {*}
     */
    get parent_list() {
        if (this.node) {
            this._parent_components = parents_component(this.node);
        }
        return this._parent_components;
    }


    /**
     * Получить ближайшего предка по его имени
     * @param name
     * @returns {*}
     */
    parent(name) {
        return get_components_by_name(this.parent_list, name)[0];
    }


    parents(name) {
        return get_components_by_name(this.parent_list, name);
    }


    /**
     * Получить ближайшего потомка по его имени
     * @param name
     * @returns {*}
     */
    child(name) {
        return get_components_by_name(this.children_list, name)[0];
    }


    childs(name) {
        return get_components_by_name(this.children_list, name);
    }
};


function get_components_by_name(env, name) {
    let result_component_list = [];

    if (!env || !env.length) {
        return result_component_list;
    }

    if (!name) {
        return env[0];
    }

    each(env, parent => {
        each(parent, component => {
            if (component.constructor.name == name) {
                result_component_list.push(component);
            }
        });
    });

    return result_component_list;
}
