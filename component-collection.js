const each = require('./helper/each');
const parse_json = require('./helper/parse-json');


function get_node_component(element) {
    let el = element;
    while(el) {
        if (el && el.tagName !== 'SCRIPT') {
            return el;
        }

        el = el.previousElementSibling;
    }
    return element.parentNode;
}


module.exports = {
    _collection: [],


    /**
     * Добавить список компонент
     * @param list_components
     */
    add_list(list_components) {
        each(list_components, component => {
            this.add(component);
        });
    },


    /**
     * Добавить компоненту
     * @param component
     */
    add(component) {
        this._collection.push(component);
    },


    /**
     * Получить компоненту
     * @param component_name
     * @returns {boolean}
     */
    get(component_name) {
        let component = false;

        each(this._collection, item => {
           if (item.name === component_name) {
               component = item;
               return false;
           }
        });

        return component;
    },


    /**
     * Найти в DOM все компоненты
     * @param parent_node
     * @param argv
     */
    prepare(parent_node, ...argv) {
        let end = null;
        let wait_all_component = new Promise(function(resolve, reject){
            end = resolve;
        });
        parent_node = parent_node || document;

        each(this._collection, item => {
            if (item.query) {
                each(parent_node.querySelectorAll(item.query()), parent => {
                    let component = this.run(item.name, parent, argv);

                    if (component) {
                        component.wait(wait_all_component);
                    }
                });
            }
        });


        each(parent_node.querySelectorAll('script'), item => {
            if (item.hasAttribute('data-component') && !item.getAttribute('data-run')) {
                let name = item.getAttribute('data-component');
                let parent = get_node_component(item);
                let options = item.innerHTML;
                let component = null;

                options = options.replace(/&gt;/g, '>');
                options = options.replace(/&lt;/g, '<');
                options = parse_json(options) || [];

                item.setAttribute('data-run', 1);
                component = this.run(name, parent, options);

                if (component) {
                    component.wait(wait_all_component);
                }
            }
        });

        end();
    },


    /**
     * Запустить компоненту
     * @param component_name
     * @param parent
     * @param options
     */
    run(component_name, parent, options=[]) {
        let component = this.get(component_name);
        if (component) {
            return new component(parent, ...options);
        }

    }
};