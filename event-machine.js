const App = require('./app');
const EventEmitter = require('./event-emitter');
const SyncRequest = require('./sync-request');
const parents_component = require('./helper/parents-component');
const each = require('./helper/each');


module.exports = class EventMachine extends SyncRequest {
    constructor() {
        super();
    }


    dispatch(event_name, params=[], callback) {
        if (params[0] instanceof EventObject) {
            super.dispatch(event_name, params);
        } else {
            new EventObject(this, event_name, params, callback);
        }
    }


    attach(event_name, handler) {
        super.attach(event_name, handler);
    }
};


class EventObject extends EventEmitter {
    constructor(component, event_name, params, callback) {
        super();

        /**
         * Первым аргументов в событии всегда будет объект самого события
         */
        params.unshift(this);


        /**
         * Функция которая будет вызвана по завершению всплытия события
         */
        this.callback = callback;


        /**
         * Курсор передвижения по стеку
         * @type {number}
         * @private
         */
        this._cursor = 0;


        /**
         * Тригер остановки вспылтия события
         * @type {boolean}
         */
        this.is_stop_propagation = false;


        /**
         * Компонент с которого всплывает событие
         */
        this.target = component;


        /**
         * Название компоненты
         */
        this.name = component.constructor.name;


        /**
         * Стек компонент по которому пройдет событие начиная от текущей компоненты
         */
        this.stack = [component];

        if (component.node) {
            this.stack = this.stack.concat(
                parents_component(component.node)
            );
        }


        /**
         * Добавляем в стек список текущих контроллеров
         */
        this.stack.push(App.current_controllers);


        /**
         * Последним в стеке добавляем приложение
         */
        this.stack.push(App);


        /**
         * Имя события
         */
        this.event_name = event_name;


        /**
         * Параметры события
         */
        this.params = params;


        /**
         * Стартуем обход по стеку событий
         */
        this.start();
    }


    /**
     * Остановить всплытие события
     */
    stop() {
        this.is_stop_propagation = true;
    }


    /**
     * Запустить/возобновить всплытие события
     */
    start() {
        let components;
        let trigger = function(component, event_name, params) {
            if (component.dispatch) {
                component.dispatch(event_name, params);
            }
        };

        this.is_stop_propagation = false;

        while (components = this.stack[this._cursor]) {
            if (this.is_stop_propagation) {
                break;
            }

            if (Array.isArray(components)) {
                each(components, component => {
                    trigger(component, this.event_name, this.params);
                });
            } else {
                trigger(components, this.event_name, this.params);
            }

            this._cursor += 1;
        }

        if (this._cursor >= this.stack.length && this.callback) {
            this.callback.apply({}, this.params);
        }
    }
};
