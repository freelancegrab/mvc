const App = require('./app');
const EventEmitter = require('./event-emitter');
const wait = require('./system/wait');


/**
 * Вызывет методы on_ready когда выполнены все запросы отпраленные в wait
 * Вызывает destructor по завершению работы страницы
 * @type {SyncRequest}
 */
module.exports = class SyncRequest extends EventEmitter {
    constructor() {
        super();

        this._run = 0;

        if (this.on_ready) {
            let event_ready_halnder = () => {
                App.remove_event('page_ready', event_ready_halnder);

                if (!this.is_run()) {
                    this.is_run(1);
                    this.on_ready();
                }
            };

            App.attach_event('page_ready', event_ready_halnder);
        }

        if (this.destructor) {
            let event_destructor_handler = () => {
                App.remove_event('page_destroy', event_destructor_handler);

                this.destructor();
                this.is_run(0);
            };

            App.attach_event('page_destroy', event_destructor_handler);
        }

        this._wait = wait();
    }


    is_run(bool=null) {
        if (bool === null) {
            return this._run;
        }

        return this._run = bool;
    }


    /**
     * Добавить задачу в список ожидания
     * @param requests
     */
    wait(...requests) {
        this._wait.add(...requests);
    }


    /**
     * Подписаться на выполнения всех задач
     * @param handler
     */
    then(handler) {
        this._wait.then(handler);
    }


    /**
     * Подписать на ошибку выполнения задач
     * @param handler
     */
    catch(handler) {
        this._wait.catch(handler);
    }


    /**
     * Возвращает ссылку на промисы
     * @returns {*}
     */
    promise() {
        return this._wait._run().catch(error => {
            console.error(error);
        });
    }
};