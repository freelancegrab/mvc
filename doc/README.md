## Содержание
* [Установка](./core/install.md)
* Приложения
    * [изоморфный SPA](apps/isomorph/spa.md)
    
* Примеры
    * [Настройка nunjucks.config](examples/nunjucks-conf.md)
    * [Цепочка вызовов через промисы](examples/requests-chain.md)
    * [Работа с роутами](examples/routes.md)
   
* [Client](./core/client.md)
* [Server](./core/server.md)
* [Page](./core/page.md)
* [App](./core/app.md)
* [Route](./core/route.md)
* [Request](./core/request.md)
* [Cookie](./core/cookie.md)
* [constants](./core/constants.md)
* MVC
    * Model
    * [View](./core/view.md)
    * [Controller](./core/controller.md)
* События
    * [Собятия приложения](./core/app-events.md)
    * [EventEmitter](./core/event-emitter.md)
    * [EventMachine](./core/event-machine.md)
* Работа с компонентами
    * [Component](./core/component.md)
    * [ObjectMachine](./core/object-machine.md)
    * [ComponentCollection](./core/component-collection.md)
* [helpers](./core/helpers.md)


В нашей архитектуре страница строится из компонент, которые являются 
самодостаточными строительными блоками. Мы преследуем принцип 
«разделяй и властвуй» при котором ни какая часть не должна 
быть особенно сложной. Из более мелких компонентов можно легко 
создавать сложные и более многофункциональные компоненты.