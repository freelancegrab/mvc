[&larr; Документация](../README.md)
# Работа с роутами
`v 2.1.0`

---


##### Hello World
```js
const Route = require('mvc/route');
const Response = require('mvc/system/system-response');

Route.add('/', function(request, resolve, reject) {
    resolve(new Response('Hello World'))
});
```


##### Контроллер для обработки урла

```js
const Route = require('mvc/route');
const Controller = require('mvc/controller');


class HomeController extends Controller {
    constructor(request) {
        super(request);

        this.response('Hello World');
    }
}

Route.add('/', HomeController);
```


##### Списко роутов

```js
Route.add_list({
    '/': HomeController,
    ...
});
```


##### Получение параметров из урла

```js
const Route = require('mvc/route');
const Controller = require('mvc/controller');

class HomeController extends Controller {
    constructor(request, parmas) {
        super(request);

        this.response(parmas.key);
    }
}
Route.add('/a/<key>/', HomeController);
```


##### Обработка ошибок

```js
Route.error_handler(ErrorHandler, 404);
```


##### Регулярные вырожения

обработка урла **/a/**
```js
Route.add(/^\/a\/$/, MultiController);
````

обработка нескольких урлов **/a/**, **/b/**, **/c/**
```js
Route.add(/^\/(a|b|c)\/$/, MultiController);
```


обработка любого урла по маске **/a/key/**
```js
Route.add(/^\/a\/(.+)\/$/, MultiController);
```

только числа в key **/a/key/**
```js
Route.add(/^\/a\/([0-9]+)\/$/, MultiController);
```

только четырехзначного числа в key **/a/key/**
```js
Route.add(/^\/a\/([0-9]{4})\/$/, MultiController);
```


