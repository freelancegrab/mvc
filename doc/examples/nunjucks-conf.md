[&larr; Документация](../README.md)
# Настройка nunjucks.config
`v 1.1.16`

---

Конфиг шаблонизатора помещается в корень проекта в файл `nunjucks.config.js` с таким
содержим
```js
const config = require('mvc/nunjucks.config');

module.exports = function(env) {
    config(env);
};
```
В конфиге приложения мы используем основной конфиг шаблонизатора из библиотеки, при подключении
которого будет доступно использование глобальных функций и фильтров из библиотеки. Так же в
конфиге приложения можно создавать свои фильтры и функции, определяя их в `addFilter` и `addGlobal`
[подробнее](https://mozilla.github.io/nunjucks/api.html#environment)

Затем необходимо использование этого конфига добавить в `server.js`
```js
const env = require('mvc/server/nunjucks-loader');

require('../nunjucks.config')(env);
```

Теперь укажем WebPack'у что для сборки шаблонов нужно использовать Nunjucks
`webpack.config.js`
```js
module: {
    loaders: {
        test: /\.(html|nunjucks)$/,
        loader: 'nunjucks-loader',
        query: {
            config: __dirname + '/nunjucks.config.js'
        }
    }
}
```
