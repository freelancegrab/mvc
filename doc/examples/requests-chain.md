[&larr; Документация](../README.md)
# Цепочка вызовов через промисы
`v 1.1.16`

---

##### Пример кода контроллера
```js
const ParentController = require('path_to_parent');
const Request = require('mvc/request');

module.exports = class SomeChildController extends ParentController {
    constructor() {
        super(
            Request.get('path/a').then(() => {
                return Request.get('path/b').then(() => {
                    return Request.get('path/c')
                })
            })
        );
    }
}
```

Для посторонеия цепочки запросов необходимо в коллбеке `.then` вернуть следующий запрос через `return`, иначе запрос
уйдет параллельно и его ответ не будет ожидаться в рендеринге.

