[назад](/doc/)

## Server

#### Server.static_path
Путь к статике (по умолчанию `/static/`)

---
 
#### prepare_page(content, context)
Предворительная обработка страниц
* content - строка с html страници
* context - данные страници

--- 

#### Server.run(port=5000, host='127.0.0.1')
Запуск сервера
* port - (число) порт на котором будет запущен сервер
* host - (строка) хост

--- 

#### Server.get_server()
Получение ссылки и старт сервера
