[назад](/doc/)

## EventMachine
Реализует всплытие события у компонент

#### EventMachine.constructor()
 
---

#### EventMachine.dispatch(event_name, params=[], callback)
Возбуждает событие и создает объект события `EventObject`
* event_name - Название события 
* params - Список параметров события
* callback - функция которая будет вызванна после окончания всплытия события

---

#### EventMachine.attach(event_name, handler)
Подписаться на событие (имплементация  [EventEmitter.attach](./event-emitter.md))

---


## EventObject

#### EventObject.constructor(component, event_name, params, callback)
* component - объект с которого началось всплытие
* event_name - название события
* params - параметры события
* callback - функция которая будет вызванна по окончании всплытия события
 
---

#### EventObject.stop()
Остановить всплытие

---

#### EventObject.start()
Начать/продолжить всплытие события
