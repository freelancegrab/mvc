[назад](/doc/)

## App

#### App.component
Ссылка на [ComponentCollection](./component-collection.md)

---

#### App.current_controllers
Список контроллеров загруженных на текущую страницу

---

#### App.set_context(context)
Установка контекста страници (доступен внутри шаблонов)
* context - объект с данными

---

#### App.get_context()
Получение объекта контекста (копии объекта)

---

#### App.clear_context()
Очистка контекста (вызывается автомотически при переходе на новую страницу)

---

#### App.property(name, value)
Устанавливаем и получаем параметра приложения
* name - название параметра
* value - значение параметра
Если value не передан вернется значение параметра

```javascript
App.property('env');
// undefined

App.property('env', 'client');
// 'client'

App.property('env');
// 'client'
```

---

#### App.dispatch_event(event_name, params=[])
Возбудить событие приложения (подробне про события в разделе [EventEmitter](./event-emitter))

---

#### App.attach_event(event_name, handler)
Подписаться на событие приложения (подробне про события в разделе [EventEmitter](./event-emitter))

---

#### App.remove_event(event_name, handler)
Удалить обработчик события приложения (подробне про события в разделе [EventEmitter](./event-emitter))

--- 

#### App.add_components(...components)
Добавить список компонент приложения

```javascript
const Component = require('mvc/component');

class MyComponent extends Component {
    constructor(node) {
        super(node);
    }
}

class MyComponent2 extends Component {
    constructor(node) {
        super(node);
    }
}

App.add_components(
    Component.init('.my-component', MyComponent),
    Component.init('.my-component', MyComponent2)
)
```

---

#### App.prepare_component(parent_node, ...argv)
Найти и инициализировать компоненты
* parent_node - родительская нода внутри которой будет происходить 
  поиск компоненты, по умолчанию `document`
* argv - аргементы которые будут переданы в конструктор компоненты 
  при инициализации
  
```javascript
const Component = require('mvc/component');

class MyComponent extends Component {
    constructor(node, custom_text) {
        super(node);
        // custom_text === 'hello world'
    }
}

App.add_components(Component.init('.my-component', MyComponent));

App.prepare_component(false, 'hello world');
```