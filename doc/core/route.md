[назад](/doc/)
## Route 

#### Route.is_last_slashes 
Обязательный последний слеш по умолчанию `True`

---

#### Route.route_list
Обект со списком всех имеющихся урлов

---

#### Route.check(url)
Проверка на существование url в списке
* url - строка вида /path/

Возвращает объект:
```javascript
{
    route: {
        path: route,
        parts: [
            {
                value: item,
                variable: ''
            }
        ],
        handler: [handler]
    },
    params: route_params,
}
```

---

#### Route.add(route, handler)

Добавление роута в список
* route - строка вида /path/to/<page>/ где <page> любой символ или строка 
* handler - функция или объект класса контроллера

Пример с передачей функции:
```javascript
Route.add('/about', function(resolve, reject){
resolve('Hello');
});
```

Пример с передачей контроллера:
```javascript
const Controller = require('mvc/controller');
const Route = require('mvc/route');

class AboutController extends Controller {
    constructor() {
        supre();
        this.render_string('Hello');
    }
}

Route.add('/about', AboutController);
```

---

#### Route.add_list(route_list)
Добавить список роутов
* route_list - объект где ключь является роутом а значение функцией или объектом класса контроллера

Пример:
```javascript
Route.add_list({
    '/': function(resolve, reject) {
        resolve('This page Home');
    },
    '/about': function(resolve, reject) {
        resolve('This page About');
    },
});
```
 
---

#### Route.path_to_parts(path)
Преобразовать урл в объект
* path - урл вида /path/to/page

Взвращает список вида:
```javascript
Route.path_to_parts('/path/to/page');
//[
//    {
//        value: 'path',
//        variable: ''
//    },
//    {
//        value: 'to',
//        variable: ''
//    },
//    {
//        value: 'page',
//        variable: ''
//    }
//]


Route.path_to_parts('/path/to/<key>');
//[
//    {
//        value: 'path',
//        variable: ''
//    },
//    {
//        value: 'to',
//        variable: ''
//    },
//    {
//        value: '',
//        variable: 'key'
//    }
//]
```

---

#### Route.check_slashes(path)
Уберает лишние слешы из пути
* path - урл вида /path/to/page