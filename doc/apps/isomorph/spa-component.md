[&larr; Документация](../../README.md)

* [Настройка окружения](./spa.md)
* [Роуты](./spa-routes.md)
* [Наследование шаблонов, разделение сервера и клиента](./spa-views.md)
* [Отправка запросов и обработка данных](./spa-request.md)
* Первая компонента
* [Сложные компоненты и работа с событиями](./spa-event-component.md)
* [Добавление хелперов и работа с шаблонизатором](./spa-template.md)


# Изоморфный SPA: Первая компонента
`v 1.1.16`

---

#### Копмпоненты в MVC

Компоненты в библиотеке строятся на базе файла шаблона(.html), файла скрипта(.js) и 
файла стиля(.scss). Можно построить компоненту только на *.js файле или *.html файле. 

В этом разделе сделаем компонент формы заявки на лот

Сделаем шаблон компоненты формы заявки `component/form/form.html`

```html
{% macro Form() %}
    <div>
        <form action="/send" method="post">
            <legend>Заявка на лот</legend>
            <fieldset>
                <div>
                    <label>ФИО</label>
                    <input type="text" name="name">
                </div>
                <div>
                    <label>Email</label>
                    <input type="email" name="email">
                </div>
                <div>
                    <button type="submit">Отправить</button>
                </div>
            </fieldset>
        </form>
    </div>
{% endmacro %}
```

Реализуем отправку формы в `component/form/form.js`

```js
const Component = require('mvc/component');
const Request = require('mvc/request');

module.exports = class Form extends Component {
    constructor(node) {
        super(node);
        
        node.addEventListener('submit', this._send.bind(this));
    }
    
    _send() {
        let action_url = this.form.action.toLowerCase();

        Request.post(action_url, {
            body: new FormData(this.node)
        }).then(response => {
            if (response.success) {
                alert('Request is done!');
            }
        });
    }
};
```

В `source/client.js` нужно добавить компоненту отправки формы в приложение

```javascript
App.component.add_list([require('./component/request-form/request-form')]);
```

В шаблоне вызовем компоненту отправку формы `component/form/form.html`

```html
{% macro Form() %}
    <div>
        <form action="/send" method="post">
            {{ run('Form') | safe }}
            <legend>Заявка на лот</legend>
            <fieldset>
                <div>
                    <label>ФИО</label>
                    <input type="text" name="name">
                </div>
                <div>
                    <label>Email</label>
                    <input type="email" name="email">
                </div>
                <div>
                    <button type="submit">Отправить</button>
                </div>
            </fieldset>
        </form>
    </div>
{% endmacro %}
```

Теперь форму импортируем в шаблон лота `views/lot/lot.html`

```html
{% extends '../../component/layout/layout.html' %}

{% from '../../component/request-form/request-form.html' import Form %}

{% block content %}

    <div class="lot">
        <h1 class="lot__ttl">{{ lot.name }}</h1>
        <div class="lot__info">
            <p class="lot__desc">{{ lot.description }}</p>
            <a href="{{ lot.external_trading }}" target="_blank" class="link">
                Внешний источник
            </a>
        </div>
        
        {{ Form() }}
        
    </div>
    
{% endblock %}
```

Можно запустить наше приложение и перейти на страницу лота. На ней теперь есть форма заявки, при успешной
отправке формы покажется алерт с сообщением "Request is done!"

---

[&larr; Отправка запросов и обработка данных](./spa-request.md) | Первая компонента | 
[Сложные компоненты и работа с событиями &rarr;](./spa-event-component.md)