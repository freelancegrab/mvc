[&larr; Документация](../../)

* Настройка окружения
* [Роуты](./spa-routes.md)
* [Наследование шаблонов, разделение сервера и клиента](./spa-views.md)
* [Отправка запросов и обработка данных](./spa-request.md)
* [Первая компонента](./spa-component.md)
* [Сложные компоненты и работа с событиями](./spa-event-component.md)
* [Добавление хелперов и работа с шаблонизатором](./spa-template.md)

# Изоморфный SPA: настройка окружения
`v 1.1.16`

---

Создадим рабочее пространство для нашего будующего приложения:

```bash
mkdir spa
cd spa
npm init
```

Настроим `package.json`

```json
{
    "name": "spa",
    "version": "1.0.0",
    "description": "",
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1",
        "dev": "webpack --verbose --colors --display-error-details --progress",
        "watch": "webpack --watch",
        "start": "nodemon source/server.js"
    },
    "author": "",
    "license": "ISC",
    "dependencies": {
        "autoprefixer": "^6.6.1",
        "babel": "^6.5.2",
        "babel-core": "^6.21.0",
        "babel-loader": "^6.2.10",
        "babel-preset-es2015": "^6.18.0",
        "css": "^2.2.1",
        "css-loader": "^0.26.1",
        "extract-text-webpack-plugin": "^1.0.1",
        "file-loader": "^0.9.0",
        "glc": "git+ssh://gitlab@gitlab.prototypes.ru:d.tsirulnikov/glc.git#master",
        "html-webpack-plugin": "^2.26.0",
        "mvc": "git+ssh://gitlab@gitlab.prototypes.ru:frontend/mvc.git#stable",
        "node-sass": "^4.1.1",
        "nodemon": "^1.11.0",
        "nunjucks": "^3.0.0",
        "nunjucks-loader": "^2.4.5",
        "postcss": "^5.2.8",
        "postcss-loader": "^1.2.1",
        "sass": "^0.5.0",
        "sass-loader": "^4.1.1",
        "style": "0.0.3",
        "style-loader": "^0.13.1",
        "webpack": "^1.14.0"
    }
}
```

И установим зависимости 

```bash
npm i
```

настроим `webpack.config.js`

```js
'use strict';

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

const extractCSS = new ExtractTextPlugin(
    addHash('[name].css', '[contenthash]'), {
        allChunks: true
    }
);

function addHash(template, hash) {
    return template + '?hash=' + hash;
}

module.exports = {
    entry: ['./source/client.js'],
    output: {
        path: __dirname + '/static/',
        publicPath: '/static/',
        filename: 'build.js'
    },
    
    watchOptions: {
        aggregateTimeout: 100
    },
    
    devtool: 'source-map',
    
    context: __dirname,
    
    postcss: function () {
        return [autoprefixer];
    },
    
    plugins: [
        extractCSS
    ],
    
    module: {
        loaders: [
            {
                test: __dirname,
                loader: 'babel-loader',
                exclude: /node_modules(\\|\/)(?![mvc|glc])/,
                include: __dirname,
                query: {
                    cacheDirectory: true,
                    presets: ['es2015']
                    
                }
            },
            {
                test: /\.(html|nunjucks)$/,
                loader: 'nunjucks-loader',
                query: {
                    config: __dirname + '/nunjucks.config.js'
                }
            },
            {
                test: /\.css$/,
                loader: extractCSS.extract(
                    'style',
                    'css?importLoaders=1' +
                    '!postcss'
                )
            },
            {
                test: /\.scss$/,
                loader: extractCSS.extract(
                    'style',
                    'css?importLoaders=2' +
                    '!postcss' +
                    '!sass'
                )
            }
        ]
    }
};
```

И сделаем первую страницу `source/server.js`
```js
const Server = require('mvc/server');
const Route = require('mvc/route');

Route.add('/', function (resolve, reject) {
    resolve('Hello world');
});

Server.run();
```

Запустим наше первое приложение 
```bash
npm start
```

Откройте страницу http://127.0.0.1:5000/ 

---

Настройка окружения | [Роуты &rarr;](./spa-routes.md)