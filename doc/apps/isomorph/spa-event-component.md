[&larr; Документация](../../README.md)

* [Настройка окружения](./spa.md)
* [Роуты](./spa-routes.md)
* [Наследование шаблонов, разделение сервера и клиента](./spa-views.md)
* [Отправка запросов и обработка данных](./spa-request.md)
* [Первая компонента](./spa-component.md)
* Сложные компоненты и работа с событиями
* [Добавление хелперов и работа с шаблонизатором](./spa-template.md)

# Изоморфный SPA: Сложные компоненты и работа с событиями
`v 1.1.16`

---

#### Компонента валидации полей формы
Форма почти готова, ей требуется валидация на заполненность полей формы.

Создадим файл `component/validate-form/validate-form.js` со следующим кодом

```js
const Component = require('mvc/component');
const each = require('mvc/helper/each');

module.exports = class ValidateForm extends Component {
    constructor(node) {
        super(node);
        
        node.form.addEventListener('submit', event => {
            event.preventDefault();
            
            let is_stop = true;
            let inputs = node.querySelectorAll('input');
            
            each(inputs, input => {
                if (input && !input.value) {
                    input.classList.add('form__error')
                } else {
                    is_stop = false;
                    input.classList.remove('form__error');
                }
            });
            if (!is_stop) {
                this.dispatch('validate');
            }
        })
    }
};
```

Этот компонент отвечает за валидацию полей формы и проверяет их заполненность. При успешной проверке создается 
событие `validate` в скопе приложения, на которое можно подписаться в компоненте отправки формы
`component/form/form.js`

```js
const Component = require('mvc/component');
const Request = require('mvc/request');

module.exports = class RequestForm extends Component {
    constructor(node) {
        super(node);
        this.attach('validate', this._send_form.bind(this));
    }
    
    _send_form() {
        Request.post('/api/external-project/', {
            body: new FormData(this.node)
        }).then(response => {
            if (response.success) {
                alert('Request is done!');
            }
        });
    };
};
```

Добавим вызов компоненты валидации в шаблон формы `component/form/form.html`

```html
{% macro Form() %}
    <div>
        <form action="/send" method="post">
            {{ run('Form') | safe }}
            
            <legend>Заявка на лот</legend>
            <fieldset>
                {{ run('ValidateForm') | safe }}
                
                <div>
                    <label>ФИО</label>
                    <input type="text" name="name">
                </div>
                <div>
                    <label>Email</label>
                    <input type="text" name="name">
                </div>
                <div>
                    <button type="submit">Отправить</button>
                </div>
            </fieldset>
        </form>
    </div>
{% endmacro %}
```

> Стоит обратить внимание: перехват события возможен только выше стоящей компонентой, сперва идет валидация полей
формы, затем только отправка всей формы

Так же необходимо добавить компоненту валидации в файл `client.js`

```js
App.component.add_list([
    require('./component/form/form'),
    require('./component/validate-form/validate-form')
]);
```

Можно запустить приложение, перейти на страницу лота и отправить форму. При заполнении полей и отправке покажется
алерт о том что форма отправлена.

---

[&larr; Первая компонента](./spa-component.md) | Сложные компоненты и работа с событиями |
[Добавление хелперов и работа с шаблонизатором &rarr;](./spa-template.md)