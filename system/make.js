const App = require('../app');
const Route = require('../route');
const constants = require('../constants');
const SystemResponse = require('./system-response');
const logger = require('../logger');
const each = require('../helper/each');
const parse_query = require('../helper/parse-query');
const system_join_response = require('./system-join-response');


module.exports = function (url, request) {
    let query;
    let promise_list = [];
    let response_list = [];

    url = url.split('?');

    query = url[1];

    App.dispatch_event('page_destroy');

    /**
     * Очищаем список текущих контроллеров
     * @type {Array}
     */
    App.current_controllers = [];

    each(Route.check(url[0]), part => {
        if (part.handlers.length) {
            part.handlers.map(handler => {

                if (handler.prototype && handler.prototype.promise) {
                    let cls = new handler(request, part.params, parse_query(query));

                    App.current_controllers.push(cls);
                    promise_list.push(cls.promise().then(response => {
                        response_list.push(cls._response);
                    }));

                } else {
                    promise_list.push(new Promise((resolve, reject) => {
                        handler(request, resolve, reject);
                    }).then(response => {
                        response_list.push(response);
                    }));
                }
            });
        }
    });

    return Promise.all(promise_list).then(() => {
        return system_join_response(response_list);
    }).catch(error => {
        logger.error(logger.CODE.PROMISE, error);
    });
};