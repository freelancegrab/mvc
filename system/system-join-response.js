const constants = require('../constants');
const SystemResponse = require('./system-response');
const each = require('../helper/each');

module.exports = function response_join(response_list) {
    let code = 0;
    let headers = {};
    let body = {
        html: [],
        json: [{}]
    };

    each(response_list, item => {
        if (item === false) {
            return;
        }

        if (item.code > code) {
            code = item.code;
            body = {
                html: [],
                json: []
            }
        }

        headers = item.headers;
        if (typeof item.body === 'string' || typeof item.body === 'number') {
            body['html'].push('' + item.body);
        } else if (typeof item.body === 'object') {
            body['json'].push(item.body);
        }
    });

    return new SystemResponse(body, code || constants.REQUEST_CODE_NOT_FOUND, headers);
};