const http = require('http');
const https = require('https');
const url = require('url');
const Request = require('../request');


let driver = {
    http: http,
    https: https
};


function request_callback(resolve, reject, res, req) {
    let response_body = [];
    let error_handler = function (err) {
        reject({
            body: err,
            response: res,
            token: req
        });
    };

    res.setEncoding('utf8');

    res.on('data', function (data) {
        response_body.push(data);
    });

    res.on('error', error_handler);
    res.on('timeout', error_handler);

    res.on('end', function () {
        let body = response_body.join('');

        if (('' + res.statusCode).match(/^2\d\d$/)) {
            resolve({
                body: body,
                response: res,
                token: req
            });
        } else {
            error_handler(body);
        }
    });

}


module.exports = function server_request(options, before_handler=false) {
    return new Promise(function (resolve, reject) {
        let url_object = url.parse(options.url);
        let protocol = url_object.protocol.replace(':', '');
        let req;
        let opt = {
            host: url_object.hostname,
            port: url_object.port,
            path: url_object.path,
            method: options.method || 'GET',
            headers: options.headers || {}
        };

        if (options.body) {
            opt.headers['Content-Length'] = Buffer.byteLength(options.body)
        }

        if (!protocol) {
            protocol = Request.protocol
        }

        req = driver[protocol].request(opt, res => {
            if (before_handler) {
                before_handler({
                    request: req,
                    response: res
                });
            }
            request_callback(resolve, reject, res, req);
        });

        if (options.body) {
            req.write(options.body);
        }

        req.end();
    });
};


/**
 * Example
 *
 * request({
 *     url: 'http://172.16.11.223:8081/api/set_play_queue',
 *
 *     method: 'post',
 *
 *     headers: {
 *         'Content-Type': 'application/json'
 *     },
 *
 *     body: JSON.stringify({ "key": "value" })
 * });
 */