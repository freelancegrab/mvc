const is_object = require('../helper/is-object');
const parse_json = require('../helper/parse-json');
const each = require('../helper/each');


module.exports = function client_request(options, before_handler=false) {
    return new Promise(function (resolve, reject) {
        let XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
        let xhr = new XHR();
        let body = options.body;

        xhr.open(options.method, options.url);

        each(Object.keys(options.headers), key => {
            xhr.setRequestHeader(key, options.headers[key]);
        });

        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) {
                return;
            }

            if (xhr.status != 200) {
                reject(xhr.responseText);
            } else {
                resolve({
                    body: xhr.responseText,
                    response: {
                        headers: {
                            'content-type': xhr.getResponseHeader('Content-Type')
                        }
                    },
                    token: xhr
                });
            }
        };

        if (is_object(body)) {
            body = parse_json(body);
        }

        if (before_handler) {
            before_handler(xhr);
        }

        xhr.send(body);
    });
};