const constants = require('../constants');
const App = require('../app');
const Route = require('../route');
const SystemResponse = require('./system-response');
const system_join_response = require('./system-join-response');


module.exports = function (request, code=constants.REQUEST_CODE_NOT_FOUND) {
    let promise_list = [];
    let response_list = [];
    let handlers = Route._error_handlers[code] || [];

    handlers.map(handler => {

        if (handler.prototype && handler.prototype.promise) {
            let cls = new handler(request, {}, {});

            App.current_controllers.push(cls);
            promise_list.push(cls.promise().then(response => {
                response_list.push(cls._response);
            }));

        } else {
            promise_list.push(new Promise((resolve, reject) => {
                handler(request, resolve, reject);
            }).then(response => {
                response_list.push(new SystemResponse(response, 200));
            }));
        }
    });

    return Promise.all(promise_list).then(() => {
        let response = system_join_response(response_list);
        response._response.code = code;

        return response;
    }).catch(error => {
        console.error(error);
    });
};