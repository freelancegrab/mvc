const constants = require('../constants');


module.exports = class SystemResponse {
    constructor(body, code, headers={}) {
        if (!headers['Content-Type']) {
            headers['Content-Type'] = (typeof body === 'object') ? constants.REQUEST_CONTENT_TYPE_JSON : constants.REQUEST_CONTENT_TYPE_HTML;
        }

        this._response = {
            code: code || constants.REQUEST_CODE_SUCCESS,
            body: body,
            headers: headers
        };
    }

    get code() {
        return this._response.code;
    }


    get body() {
        return this._response.body;
    }


    get headers() {
        return this._response.headers;
    }


    get_content(type) {
        let content = '';

        if (this.body.json || this.body.html) {
            if (this.body.json.length && this.body.html.length) {
                if ([constants.REQUEST_CONTENT_TYPE_JSON].includes(type)) {
                    content = JSON.stringify(Object.assign(...this.body.json));
                } else {
                    content = this.body.html.join('');
                }
            } else {
                content = this.body.html.length ? this.body.html.join('') : JSON.stringify(Object.assign(...this.body.json));
            }
        }

        return content;
    }
};