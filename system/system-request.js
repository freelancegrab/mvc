const App = require('../app');
const constants = require('../constants');
const parse_json = require('../helper/parse-json');
const parse_query = require('../helper/parse-query');
const each = require('../helper/each');


module.exports = class SystemRequest {

    constructor(request={}, body='') {
        this.body_object = null;
        this._type = null;

        this._request = {
            url: request.url,
            method: request.method,
            referer: request.referer,
            body: body,
            headers: Object.assign({
                cookie: '',
                referer: '',
                'Content-Type': ''
            }, request.headers)
        };

        this._cookie = new RequestCookie(request.headers.cookie);
    }


    get cookie() {
        return this._cookie;
    }


    get url() {
        return this._request.url;
    }


    get referer() {
        return this._request.referer;
    }


    get method() {
        return this._request.method;
    }


    get body() {
        if (!this.body_object) {
            this.body_object = parse_json(this._request.body);

            if (!Object.keys(this.body_object).length) {
                this.body_object = parse_query(this._request.body);
            }
        }

        return this.body_object;
    }


    get body_string() {
        return this._request.body
    }


    get headers() {
        this._request.headers.cookie = this.cookie.toString();
        return this._request.headers;
    }


    get type() {
        if (!this._type) {
            if (this._request.headers['Content-Type']) {
                this._type = this._request.headers['Content-Type'];
            } else {
                this._type = constants.REQUEST_CONTENT_TYPE_TEXT;
            }
        }

        return this._type;
    }
};


class RequestCookie {
    constructor(cookie_string='') {
        this._list = {};

        this._replace(cookie_string);
    }


    toList() {
        let string = [];

        each(Object.keys(this._list), key => {
            string.push(this._list[key].string);
        });

        return string;
    }


    toString() {
        return this.toList().join('; ');
    }


    set(key, value, options={}) {
        if (key) {
            App.dispatch_event('set_cookie', [
                this._push(key, value, options)
            ]);
        } else {
            throw 'Cookie key is undefined';
        }
    }


    get(cookie_name) {
        return this._list.hasOwnProperty(cookie_name) ? this._list[cookie_name].value : false;
    }


    remove(cookie_name) {
        App.dispatch_event('remove_cookie', [
            this._push(cookie_name, '', {
                expires: 'Thu, 01 Jan 1970 00:00:01 GMT'
            })
        ]);
    }


    _push(key, value='', options={}) {
        let string;

        key = key.trim();
        value = value.trim().replace(/;$/, '');
        string = [key + '=' + value];

        if (!options['path']) {
            options['path'] = '/'
        }

        each(Object.keys(options), key => {
            let value = options[key];

            if (key === 'expires') {
                let date;
                if (isNaN(value)) {
                    date = new Date(value);
                } else {
                    date = new Date(value);
                    date = date.setDate(date.getDate() + value);
                }
                value = date.toUTCString();
            }

            string.push(key + '=' + value);
        });

        return this._list[key] = {
            key: key,
            value: value,
            options: options,
            string: string.join('; ')
        };
    }

    _replace(cookie_string) {
        this._list = {};

        if (!cookie_string) {
            return;
        }

        each(cookie_string.split(' '), item => {
            let key = item.substr(0, item.indexOf('='));
            let value = item.substr(item.indexOf('=') + 1);
            this._push(key.trim(), value.trim());
        });
    }
}