require('yaml-js');

const each = require('../helper/each');
const is_array = require('../helper/is-array');
const is_object = require('../helper/is-object');


module.exports = function (...argv) {
    let config = {};
    each(argv, item => {
        if (is_object(item)) {
            Object.assign(config, item);
        } else {
            try {
                let conf = require(item);
                if (is_array(conf)) {
                    each(conf, param => {
                        Object.assign(config, param);
                    });
                } else {
                    Object.assign(config, conf);
                }
            } catch (e) {}
        }
    });

    return config
};