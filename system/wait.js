module.exports = function (...requests) {
    return new WaitPromise(...requests);
};

class WaitPromise {
    constructor(...requests) {
        this._reference = null;
        this._requests = requests;
        this._requests_length = this._requests.length;
    }

    _run() {
        if (!this._reference) {
            this._reference = Promise.all(this._requests);
        }

        return this._reference;
    }


    add(...requests) {
        this._reference = null;
        this._requests = this._requests.concat(requests);
    }

    then(handler) {
        this._run().then(handler);
    }

    catch(handler) {
        this._run().catch(handler);
    }
}