const App = require('./app');
const EventEmitter = require('./event-emitter');
const constants = require('./constants');
const logger = require('./logger');
const dict_to_query_string = require('./helper/dict-to-query-string');
const each = require('./helper/each');
const parse_json = require('./helper/parse-json');
const is_object = require('./helper/is-object');
const is_string = require('./helper/is-string');
const is_array = require('./helper/is-array');


const RequestEvents = new EventEmitter();


module.exports = {

    _protocol: false,

    _server_request: null,

    _cli: null,

    _filter_list: [],

    _proxy_list: [],


    default_options: {
        body: {},
        headers: {}
    },

    get protocol() {
        return this._protocol;
    },


    set protocol(type) {
        this._protocol = type;
    },


    setup(request, default_options={}) {
        Object.assign(this.default_options, default_options);
        this._server_request = request;
    },


    set_proxy(from, to) {
        this._proxy_list.push({
            from: from,
            to: to
        });
    },


    set_filter(path, handler) {
        let pattern;

        if (path instanceof RegExp) {
            pattern = path;
        } else {
            pattern = new RegExp(path);
        }

        if (!handler) {
            return;
        }

        this._filter_list.push({
            pattern: pattern,
            handler: handler
        });
    },


    get_path(path) {
        let result = path
        each(this._proxy_list, item => {
            if (path.indexOf(item.from) === 0) {
                result = item.to + path.replace(item.from, '');
                return 0;
            }
        });
        return result;
    },


    merge_options(options={}) {
        let cookie;
        let headers = {};

        if (is_object(options.body) || is_string(options.body)) {
            headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        }


        if (App.property('space') === constants.SPACE_SERVER) {
            cookie = this._server_request ? this._server_request.cookie.toString() : '';

            if (cookie) {
                headers["cookie"] = cookie;
            }
        }

        headers = Object.assign(headers, this.default_options.headers);

        if (options.headers) {
            each(Object.keys(options.headers), key => {
                headers[key] = options.headers[key];
            });
        }

        options.headers = headers;

        return Object.assign({}, this.default_options, options);
    },


    get(path, options={}, before_handler=false) {
        return this._make(path, 'get', options, before_handler);
    },


    post(path, options={}, before_handler=false) {
        return this._make(path, 'post', options, before_handler);
    },


    put(path, options={}, before_handler=false) {
        return this._make(path, 'put', options, before_handler);
    },


    delete(path, options={}, before_handler=false) {
        return this._make(path, 'delete', options, before_handler);
    },


    attach_event: RequestEvents.attach.bind(RequestEvents),
    dispatch_event: RequestEvents.dispatch.bind(RequestEvents),
    remove_event: RequestEvents.remove.bind(RequestEvents),


    _merge_body(body) {
        if (this.default_options.body) {
            if (is_object(this.default_options.body) && is_object(body)) {
                body = Object.assign({}, this.default_options.body, body);
            }

            if (is_string(this.default_options.body) && is_string(body)) {
                body = [this.default_options.body, body].join('\n');
            }
        }

        return body || this.default_options.body;
    },


    _make(path, method, options, before_handler=false) {
        let env = App.property('env');
        let space = App.property('space');

        if (space === constants.SPACE_CLIENT) {
            if ([constants.ENV_SERVER, constants.ENV_ISOMORPHIC].includes(env)) {
                return new Promise((resolve, reject) => {
                    resolve({});
                });
            }
        }

        options.body = this._merge_body(options.body);

        options = this._filter(
            path,
            this.merge_options(Object.assign({method: method}, options))
        );

        if (method === 'get') {
            if (is_object(options.body)) {
                if (Object.keys(options.body).length) {
                    if (options.url.includes('?')) {
                        options.url = options.url.substr(0, options.url.indexOf('?'));
                    }
                    options.url += '?' + dict_to_query_string(options.body);
                }
            }
            if (App.property('space') === constants.SPACE_CLIENT) {
                if (options.body instanceof FormData) {
                    let data = [];

                    each(options.body.entries(), item => {
                        data.push(item[0] + '=' + item[1]);
                    });

                    if (data.length) {
                        options.url += '?' + data.join('&');
                    }
                }
            }
            delete options.body;
        } else {
            if (is_object(options.body)) {
                options.body = dict_to_query_string(options.body);
            }
        }

        each(Object.keys(options.headers), key => {
            if (options.headers[key] === null || options.headers[key] === undefined) {
                delete options.headers[key];
            }
        });

        logger.message(
            logger.CODE.REQUEST,
            logger.style('Send request', logger.COLOR.GREEN),
            logger.style(options.url, logger.COLOR.CYAN),
            JSON.stringify(options)
        );

        return this._cli(options, before_handler).then(request_object => {
            let type = request_object.response.headers['content-type'];
            let cookies = parse_cookie_set(request_object.response.headers['set-cookie']);
            let data;

            if (type.indexOf('json') > -1) {
                data = parse_json(request_object.body);
            } else {
                data = request_object.body;
            }

            RequestEvents.dispatch(method, [data, request_object, cookies]);

            logger.message(
                logger.CODE.REQUEST,
                logger.style('Result request', logger.COLOR.GREEN),
                logger.style(options.url, logger.COLOR.CYAN),
                request_object.body
            );

            return data;
        });
    },


    _filter(path, options) {
        each(this._filter_list, item => {
            if (item.pattern.test(path)) {
                let result = item.handler(path, options);

                if (is_array(result) && result.length > 1) {
                    path = result[0];
                    options = result[1];
                } else if (is_object(result)) {
                    options = result;
                }
            }
        });
        options.url = this.get_path(path);
        return options;
    }
};


function parse_cookie_set(cookie_list) {
    let cookies = [];

    each(cookie_list, cookie_string => {
        let cookie = cookie_string.split(';').shift();

        each(cookie.split(' '), item => {
            let key = item.substr(0, item.indexOf('='));
            let value = item.substr(item.indexOf('=') + 1);

            if (key && value) {
                cookies.push({
                    key: key.trim(),
                    value: value.trim()
                });
            }
        });
    });

    return cookies;
};
