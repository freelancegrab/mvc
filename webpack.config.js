'use strict';

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');


const extractCSS = new ExtractTextPlugin(
    addHash('[name].css', '[contenthash]'), {
        allChunks: true
    }
);

function addHash(template, hash) {
    return template + '?hash=' + hash;
}

module.exports = {
    entry: ['./source/index.js'],
    output: {
        path: __dirname + '/source/bin/',
        publicPath: '/source/bin/',
        filename: 'bundle.js'
    },

    postcss:  function () { return [autoprefixer]; },
    watchOptions: { aggregateTimeout: 100 },
    context: __dirname,

    plugins: [ extractCSS ],

    devtool: 'source-map',

    module: {
        loaders: [
            {
                test: __dirname,
                loader: 'babel',
                exclude: /node_modules/,
                query: {
                    cacheDirectory: true,
                    presets: [ 'es2015' ]
                }
            },
            {
                test: /\.(html|nunjucks)$/,
                loader: 'nunjucks-loader',
                query: {}
            },
            {
                test: /\.css$/,
                loader: extractCSS.extract(
                    'style',
                    'css?importLoaders=1' +
                    '!postcss'
                )
            },
            {
                test: /\.scss$/,
                loader: extractCSS.extract(
                    'style',
                    'css?importLoaders=2' +
                    '!postcss' +
                    '!sass'
                )
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file?hash=sha512&digest=hex&name=img/[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            }
        ]
    }
};
