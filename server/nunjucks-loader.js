const nunjucks = require('nunjucks');
const env = new nunjucks.Environment(new nunjucks.FileSystemLoader);

nunjucks.configure('/', {
    autoescape: true,
});


function registerTplExtensions(module, filename) {
    module.exports = {
        render: function (context) {
            return env.render(filename, context);
        }
    }
}

require.extensions['.html'] = registerTplExtensions;
require.extensions['.njk'] = registerTplExtensions;
require.extensions['.nunjucks'] = registerTplExtensions;


require.extensions['.svg'] = function (module, filename) {
    module.exports = fs.readFileSync(module, 'utf8');
};


module.exports = env;