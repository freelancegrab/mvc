module.exports = {
    /**
     * Пространство выполнения на клиенте
     */
    ENV_CLIENT: 'client',


    /**
     * Пространство выполнения на сервере
     */
    ENV_SERVER: 'server',


    /**
     * Изоморфное приложение
     */
    ENV_ISOMORPHIC: 'isomorphic',


    /**
     * Пространство выполнение: клиент
     */
    SPACE_CLIENT: 'client',


    /**
     * Пространство выполнения: сервер
     */
    SPACE_SERVER: 'server',


    REQUEST_CODE_SUCCESS: 200,
    REQUEST_CODE_REDIRECT: 304,
    REQUEST_CODE_NOT_FOUND: 404,


    REQUEST_CONTENT_TYPE_TEXT: 'text/plain',
    REQUEST_CONTENT_TYPE_HTML: 'text/html',
    REQUEST_CONTENT_TYPE_XML: 'application/xml',
    REQUEST_CONTENT_TYPE_JSON: 'application/json'
};