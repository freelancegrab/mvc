const App = require('./app');
const View = require('./view');
const EventEmitter = require('./event-emitter');
const SystemResponse = require('./system/system-response');
const SyncRequest = require('./sync-request');
const constants = require('./constants');
const wait = require('./system/wait');


module.exports = class Controller extends SyncRequest {
    constructor() {
        super();

        /**
         * Данные контролера для рендеринга
         * @type {{}}
         */
        this.scope = Object.assign({}, App._scope);


        this._response = new SystemResponse('', 200);
    }


    /**
     * Получение установка данных контекста контроллера
     * @param name
     * @param value
     * @returns {*}
     */
    property(name, value) {
        return this.scope[name] = value !== undefined ? value : this.scope[name];
    }


    /**
     * Отрендерить шаблон
     * @param template
     * @param context
     */
    render(template, context={}) {
        this.then(response => {
            let content;
            response.unshift(this.scope);
            response.push(context);

            Object.assign(...response);

            App.set_context(this.scope);

            if (App.property('space') === constants.SPACE_SERVER) {
                content = View.render(template, this.scope);
            } else {
                if (App.property('env') === constants.ENV_CLIENT) {
                    content = View.render(template, this.scope);
                }
            }

            this._change_response(new SystemResponse(content));
        });
    }


    /**
     * Вернуть строку
     * @param string
     */
    render_string(string) {
        this.then(response => {
            this._change_response(new SystemResponse(string));
        });
    }


    /**
     * Переход по страницам
     * @param response
     */
    url_for(response) {
        this.then(() => {
            this._change_response(response);
        });
    }


    /**
     * Сформировать свйот ответ от сервера
     * @param body
     * @param code
     * @param headers
     */
    response(body, code, headers) {
        this.then(response => {
            this._change_response(new SystemResponse(body, code, headers));
        });
    }


    /**
     * Возбуждает ошибку на странице
     * @param code
     */
    abort(code) {
        this.then(response => {
            this._change_response(new SystemResponse('', code));
        });
    }


    /**
     * Изменение ответа приложения
     * @param response
     * @private
     */
    _change_response(response) {
        if (this._response) {
            if (this._response.code > response.code) {
                return;
            }
        }

        this._response = response
    }
};