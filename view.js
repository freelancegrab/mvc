module.exports = {
    /**
     * Рендер шаблона
     * @param template
     * @param context
     * @returns {*}
     */
    render(template, context={}) {
        if (template) {
            return template.render ? template.render(context) : template(context);
        }
        return '';
    }
};
